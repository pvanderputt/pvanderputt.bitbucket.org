




var index = 0;
var carouselLength;
var carouselInterval = 4000;
var carouselTimer;
var url = "carousel.txt";


function MakeCarousel(data){
	//console.log(data);
	var images = data.images;

	var carouselC = $("<div class='carousel'></div>");
	$(".container").append(carouselC);
	var list = $("<ul></ul>");
	carouselC.append(list);

	var first = true;

	for(var i in images){
		//console.log(images[i]);
        
		var li = $("<li></li>").attr("id", i);
        var img = $("<img></img>").attr("src", images[i].url).attr("alt", images[i].alt);
		if (first) {
		    li.addClass("show");
		    first = false;
		}
		else {
		    li.addClass("hide");
		}
		li.append(img);
		list.append(li);
	}

	var left = $("<a></a>");

	$("body").keyup(function (event) {
	    if (event.which == 65 || event.which == 37) {
	        clearInterval(carouselTimer);
	        TurnCarousel("left");
	    }
	    if (event.which == 68 || event.which == 39) {
	        clearInterval(carouselTimer);
	        TurnCarousel("right");
	    }
	});

	
}

function SetupCarousel(data) {
    
    MakeCarousel(data);

    carouselLength = $(".carousel li").length;

	carouselTimer = setInterval(TurnCarousel, carouselInterval);
}

function TurnCarousel(direction) {
    clearInterval(carouselTimer);

    var right = true;

    if (direction === "left") {
        right = false;
        direction = "right";
    } else if (direction === "right") {
        right = true;
        direction = "left";
    } else {
        right = true;
        direction = "left";
    }

    $("#" + index.toString()).toggle("slide", {
        direction: direction, complete: function () {
            $(this).toggleClass("show");
            $(this).toggleClass("hide");
        }
    }, 1000);

    if (right) {
        if (index >= carouselLength - 1) {
            index = 0;
        } else {
            ++index;
        }
        direction = "right";
    } else {
        if (index <= 0) {
            index = carouselLength - 1;
        } else {
            --index;
        }
        direction = "left";
    }

    $("#" + index.toString()).toggle("slide", {
        direction: direction, complete: function () {
            $(this).toggleClass("show");
            $(this).toggleClass("hide");

            carouselTimer = setInterval(TurnCarousel, carouselInterval);
        }
    }, 1000);
}


$("document").ready(function(){
	$.getJSON(url, SetupCarousel);


	$("form").on("submit", function (e) {
	    e.preventDefault();

	    var url = "http://192.168.60.183:9001/products";
	    var data = $(this).serialize();
	    console.log(data);

	    $.post(url, data);
	});

});



/*$("document").ready(function(){
  var carousel = $("carousel");
  var carouselChild = carousel.find("li");
  
  var itemWidth = carousel.find("li:first").width()+1;
  
  carousel.width(itemWidth * carouselChild.length);
  
  RefreshChildLocation();
  
  setInterval(function(){
    carousel.stop(false,true).animate({"left": "-=" + itemWidth}, 300, function(){
      lastItem = carousel.find("li:first");
      lastItem.remove().appendTo(carousel);
      lastItem.css("left", ((carouselChild.length-1) *(itemWidth)));
    })
  }, 5000);
  
  function RefreshChildLocation(){
    carouselChild.each(function(){
      $(this).css("left", itemWidth * carouselChild.index($(this)));
    });
  }
  
});*/


/*$(function(){
			var carousel = $('.carousel ul');
			var carouselChild = carousel.find('li');
			var clickCount = 0;
			var canClick = true;
			
			itemWidth = carousel.find('li:first').width()+1; //Including margin

			//Set Carousel width so it won't wrap
			carousel.width(itemWidth*carouselChild.length);

			//Place the child elements to their original locations.
			refreshChildPosition();
			
			//Set the event handlers for buttons.
			$('.btnNext').click(function(){
				if(canClick){
					canClick = false;
					//clickCount++;
					
					//Animate the slider to left as item width 
					carousel.stop(false, true).animate({
						left : '-='+itemWidth
					},300, function(){
						//Find the first item and append it as the last item.
						lastItem = carousel.find('li:first');
						lastItem.remove().appendTo(carousel);
						lastItem.css('left', ((carouselChild.length-1)*(itemWidth)));
						canClick = true;
					});
				}
			});
			
			$('.btnPrevious').click(function(){
				if(canClick){
					canClick = false;
					clickCount--;
					//Find the first item and append it as the last item.
					lastItem = carousel.find('li:last');
					lastItem.remove().prependTo(carousel);
	
					lastItem.css('left', itemWidth*clickCount);				
					//Animate the slider to right as item width 
					carousel.finish(true).animate({
						left: '+='+itemWidth
					},300, function(){
						canClick = true;
					});
				}
			});

			function refreshChildPosition(){
				carouselChild.each(function(){
					$(this).css('left', itemWidth*carouselChild.index($(this)));
				});
			}
		});
*/




/*
$("document").ready(function(){
	var t = setInterval(function(){
		$(".carousel ul").animate({marginLeft:-480},1000,function(){
			$(this).find("li:last").after($(this).find("li:first"));
			$(this).css({marginLeft:0});
		})
	}, 5000);
});
*/

/*var autoSlide;
var carouselSize = $(".carouselImage").length;
var carouselIndex = 0;
var carouselWidth = $(".carousel").width();

$("document").ready(function(){
	
	$("carouselMain").width(carouselSize * carouselWidth);
	
	autoSlide = setInterval(function(){
		TurnCarousel("right");
	}, 1000);
});

function TurnCarousel(direction){
	/*$(".carousel ul").animate({marginLeft:-480},1000,function(){
		$(this).find("li:last").after($(this).find("li:first"));
		$(this).css({marginLeft:0});
	});
	
	var carouselImages = $(".carouselImage");
	
	//var left_offset = 1 * carouselWidth;
	//carouselImages.animate({ "left": "" }, 250);
	carouselIndex++;
}*/


// on ready do setup

/*$(document).ready(function(){
  InitCarousel();
});

var carouselSize = $(".carouselImage").length;
var carouselIndex = 0;
var carouselWidth = $(".carousel").width();

function InitCarousel(){
  // put the last item in front of the first item
  $("#carouselMain li:first").before($("#carouselMain li:last"));
  $("carouselMain").width(carouselSize * carouselWidth);
  
  var images = $(".carouselImage");
  
  
  var carouselSpin = setInterval(function(){
    
    var itemWidth = $("#carouselMain li").outerWidth();
    var leftIndent = parseInt($("#carouselMain").css("left")) - itemWidth;
    console.log(itemWidth + "\t" + leftIndent + "\t")
    
    // animate the sliding effect
    $("#carouselMain").animate({"left": leftIndent}, {queue:false, duration:500}, function(){
      // put the first item in the list after the last item in the list
      $("#carouselMain li:last").after($("#carouselMain li:first"));
      $("carouselMain").css({"left" : "-720px"});
    });
    
  }, 5000);
}


$(document).ready(function() {  
        //move the last list item before the first item. The purpose of this is if the user clicks previous he will be able to see the last item.  
        $('#carousel_ul li:first').before($('#carousel_ul li:last'));  
  
        //when user clicks the image for sliding right  
        $('#right_scroll img').click(function(){  
  
            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '  
            var item_width = $('#carousel_ul li').outerWidth() + 10;  
  
            //calculate the new left indent of the unordered list  
            var left_indent = parseInt($('#carousel_ul').css('left')) - item_width;  
  
            //make the sliding effect using jquery's anumate function '  
            $('#carousel_ul').animate({'left' : left_indent},{queue:false, duration:500},function(){  
  
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '  
                $('#carousel_ul li:last').after($('#carousel_ul li:first'));  
  
                //and get the left indent to the default -210px  
                $('#carousel_ul').css({'left' : '-210px'});  
            });  
        });  
  
        //when user clicks the image for sliding left  
        $('#left_scroll img').click(function(){  
  
            var item_width = $('#carousel_ul li').outerWidth() + 10;  
  
            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */  
        /*    var left_indent = parseInt($('#carousel_ul').css('left')) + item_width;  
  
            $('#carousel_ul').animate({'left' : left_indent},{queue:false, duration:500},function(){  
  
            /* when sliding to left we are moving the last item before the first item */  
           // $('#carousel_ul li:first').before($('#carousel_ul li:last'));  
  
            /* and again, when we make that change we are setting the left indent of our unordered list to the default -210px */  
   /*         $('#carousel_ul').css({'left' : '-210px'});  
            });  
  
        });  
  });  */

