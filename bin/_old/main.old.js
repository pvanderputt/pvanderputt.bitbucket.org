var CANVAS;
var gl;

function InitGL(canvas) {
     /* Get the WebGL context */
    try{
        gl = CANVAS.getContext("experimental-webgl", {antialias: true});
    } catch(e){
        alert("No WebGL here :(");
        return;
    };
}

var fragment_shader_source="\n\
attribute vec2 position; //the position of the point\n\
void main(void) { //pre-built function\n\
gl_Position = vec4(position, 0., 1.); //0. is the z, and 1 is w\n\
}";

var vertex_shader_source="\n\
precision mediump float;\n\
\n\
\n\
\n\
void main(void) {\n\
gl_FragColor = vec4(0.,0.,0., 1.); //black color\n\
}";

var get_shader = function(source, type, typeString){
    var shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("Error loading shader: " + gl.getShaderInfoLog(shader));
        return false;
    }
    return shader;
};

var vertex_shader;
var fragment_shader;
var SHADER_PROGRAM;

function InitShaders(){
    vertex_shader = get_shader(vertex_shader_source, gl.VERTEX_SHADER, "VERTEX");
    fragment_shader = get_shader(fragment_shader_source, gl.FRAGMENT_SHADER, "FRAGMENT");
    
    SHADER_PROGRAM = gl.createProgram();
    gl.attachShader(SHADER_PROGRAM, vertex_shader);
    gl.attachShader(SHADER_PROGRAM, fragment_shader);
    
    gl.linkProgram(SHADER_PROGRAM);
    
    var _position = gl.getAttribLocation(SHADER_PROGRAM, "position");
    var _color= gl.getAttribLocation(SHADER_PROGRAM, "color");
    
    gl.enableVertexAttribArray(_position);
    gl.enableVertexAttribArray(_color);
    gl.useProgram(SHADER_PROGRAM);
}

var triangle_vertex=[
    -1,-1, //first summit -> bottom left of the viewport
    1,-1, //bottom right of the viewport
    1,1,  //top right of the viewport
  ];
    
    var TRIANGLE_VERTEX= GL.createBuffer ();
    GL.bindBuffer(GL.ARRAY_BUFFER, TRIANGLE_VERTEX);
    GL.bufferData(GL.ARRAY_BUFFER,
    new Float32Array(triangle_vertex),
    GL.STATIC_DRAW);
    
    //FACES :
    var triangle_faces = [0,1,2];
  var TRIANGLE_FACES= GL.createBuffer ();
  GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, TRIANGLE_FACES);
  GL.bufferData(GL.ELEMENT_ARRAY_BUFFER,
                new Uint16Array(triangle_faces),
                GL.STATIC_DRAW);
  
var draw = function() {
    gl.viewport(0,0, 0,0, CANVAS.width, CANVAS.height);
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    gl.vertexAttribPointer(_position, 2, gl.FLOAT, false, 4*(2+3), 0);
    gl.vertexAttribPointer(_color, 3, gl.FLOAT, false, 4*(2+3), 2*4);
    gl.bindBuffer(gl.ARRAY_BUFFER, TRIANGLE_VERTEX);
    
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, TRIANGLE_FACES);
    gl.drawElements(gl.TRIANGLES, 3, gl.UNSIGNED_SHORT, 0);
    gl.flush();
    
    window.requestAnimationFrame(draw);
}


var main = function(){
    CANVAS = document.getElementById("main_canvas");
    CANVAS.width = window.innerWidth;
    CANVAS.height = window.innerHeight;
    
    InitGL(CANVAS);
    InitShaders();
    
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    draw();
}