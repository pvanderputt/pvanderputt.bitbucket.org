

function DoAjax(e){
	var url = "ajax_json_test.json";
	
	// step 1
	var httpRequest
	if(window.XMLHttpRequest){
		httpRequest = new XMLHttpRequest();
	} else if(window.ActiveXObject){
		httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	if(!httpRequest){
		return;
	}
	
	httpRequest.addEventListener("readystatechange", CheckRequest, false);
	
	// step 2
	httpRequest.open("GET", url);
	
	// step 4
	httpRequest.send();
}

function CheckRequest(e){
	// step 3
	if(this.readyState === 4){
		console.log(JSON.parse(this.responseText));
		
		if(this.status === 200){
			console.log(this.responseText);
		} else {
			console.log("It broke somehow.\nHTTP status: " + this.status);
		}
	}
}


window.addEventListener("load", function(){
	
	document.getElementById("doAjax").addEventListener("click", DoAjax, false);
	
}, false);


function ChangeTab(e){
	var tabID = this.id + "content";
	
	var tabContent = document.getElementById(tabID);
	
	tabContent.className = "tabContent";
}

function HideTabs(e){
	if(e.target.className === "tabTitle"){
		var tabs = document.getElementsByClassName("tabContent");
		for(var i in tabs){
			if(tabs[i].id !== (e.target.id + "content")){
				tabs[i].className = "tabContent hide";
			}
		}
	}
}

/*

var openTab = undefined;
var closeTab = undefined;

function OpenAccordianTab(e){
	var content = this.parentElement.lastElementChild;
	
	content.style.display = "block";
	content.style.height = "0px";
	openTab = content;
}

function HideAccordianTabs(e){
	if(e.target.className === "accordianTabTitle" || e.target.parentElement.className === "accordianTabTitle"){
		var tabs = this.children;
		for(var i in tabs){
			//if(tabs[i].firstElementChild === e.target){
			//	closeTab = tabs[i].lastElementChild;
			//}
			tabs[i].lastElementChild.style.display = "none";
		}
	}
}

var openTabLoc = 0;
var closeTabLoc = 100;

setInterval(function(){
	var tabSpeed = 10;
	if(openTab){
		openTabLoc += tabSpeed;
		if(openTabLoc < 100){
			openTab.style.height = openTabLoc + "px";
		} else if (openTabLoc >= 100){
			openTab.style.height = "100px";
			openTab = undefined;
			openTabLoc = 0;
			for(var i in openTab.children){
				openTab.children[i].style.visibility = "visible";
			}
		}
	}
	
	if(closeTab){
		closeTabLoc -= tabSpeed;
		if(closeTabLoc > 0){
		} else if (closeTabLoc <= 0){
			closeTab.style.height = "0px";
			closeTab = undefined;
			closeTabLoc = 100;
			for(var i in closeTab.children){
				closeTab.children[i].style.visibility = "hidden";
			}
		}
	}
	
}, 16);


// display: block
// height interval things

*/

window.addEventListener("load", function(){
	// Add listeners for the tabs
	var tabs = document.getElementById("tabs")
	tabs.addEventListener("click", HideTabs, true);
	for(var i = 0; i < tabs.children.length; i++){
		tabs.children[i].firstElementChild.addEventListener("click", ChangeTab, true);
	}
	/*
	// Add listeners for the accordian
	var accordian = document.getElementById("accordian");
	var accordianTabs = document.getElementsByClassName("accordianTabTitle");
	
	accordian.addEventListener("click", HideAccordianTabs, true);
	for(var i in accordianTabs){
		accordianTabs[i].addEventListener("click", OpenAccordianTab, true);
	}
	*/
}, false);





/*
function ChangeColor(event){
	if(event.target.checked){
		event.target.parentElement.parentElement.style.backgroundColor = "blue";
	} else {
		event.target.parentElement.parentElement.style.backgroundColor = "red";
	}
}

function DeleteRow(event){
	var rowElement = event.target.parentElement.parentElement;
	rowElement.parentElement.removeChild(rowElement);
}

function AddRow(){
	var table = document.getElementById("expandableTable");
	
	var numRows = table.rows.length;
	var newRow = table.insertRow(numRows);
	
	newRow.className = "tableRow";
	
	var col1 = newRow.insertCell(0);
	var checkElement = document.createElement("input");
	checkElement.type = "checkbox";
	checkElement.addEventListener("change", ChangeColor, false);
	col1.appendChild(checkElement);
	
	var col2 = newRow.insertCell(1);
	var btnElement = document.createElement("button");
	btnElement.appendChild(document.createTextNode("Delete Row"));
	btnElement.addEventListener("click", DeleteRow, false);
	col2.appendChild(btnElement);
}

var button = document.getElementById("addRowButton");
button.addEventListener("click", AddRow, false);

var rows = document.getElementsByClassName("tableRow");
for(var i = 0; i < rows.length; i++){
	rows[i].children[0].children[0].addEventListener("change", ChangeColor, false);
	rows[i].children[1].children[0].addEventListener("click", DeleteRow, false);
}
*/


/*
var ball = document.getElementById("bouncingDiv");
var ballImg = document.getElementById("divImg");
var wtop = 0;
var wbottom = window.innerHeight - (ball.clientHeight);
var wleft = 0;
var wright = window.innerWidth - (ball.clientWidth);


var ballXPos = (Math.random())*wright - 1;
var ballYPos = (Math.random())*wbottom - 1;
var ballXVel = 4;
var ballYVel = 3;

var ballRot = 0;
var ballRotVel = 5;

var score = 0;
var imgPath = "bin/assets/images/Ball_";

var wallHitSound = new Howl({
	urls:["bin/assets/audio/ding-happy.mp3"]
});

function MoveBall(){
	ballXPos += ballXVel;
	ballYPos += ballYVel;
	ballRot += ballRotVel;
}

function ConstrainBall(){
	if(ballYPos <= wtop){
		ballYPos = wtop;
		ballYVel *= -1;
		
		if(ballXVel > 0 && ballRotVel < 0){
			ballRotVel *= -1;
		} else if(ballXVel < 0 && ballRotVel > 0){
			ballRotVel *= -1;
		}
		wallHitSound.play();
	} else if (ballYPos >= wbottom){
		ballYPos = wbottom;
		ballYVel *= -1;
		
		if(ballXVel > 0 && ballRotVel < 0){
			ballRotVel *= -1;
		} else if(ballXVel < 0 && ballRotVel > 0){
			ballRotVel *= -1;
		}
		wallHitSound.play();
	}
	
	if(ballXPos <= wleft){
		ballXPos = wleft;
		ballXVel *= -1;
		
		if(ballYVel > 0 && ballRotVel > 0){
			ballRotVel *= -1;
		} else if(ballYVel < 0 && ballRotVel < 0){
			ballRotVel *= -1;
		}
		wallHitSound.play();
	} else if (ballXPos >= wright){
		ballXPos = wright;
		ballXVel *= -1;
		
		if(ballYVel > 0 && ballRotVel < 0){
			ballRotVel *= -1;
		} else if(ballYVel < 0 && ballRotVel > 0){
			ballRotVel *= -1;
		}
		wallHitSound.play();
	}
}

function UpdateBall(){
	ball.style.transform = "rotate(" + ballRot + "deg)";
	ball.style.top = ballYPos + "px";
	ball.style.left = ballXPos + "px";
}

function ScorePlus(){
	score++;
	
	if(score > 10){
		score = 0;
	}
	
	ballImg.setAttribute("src", imgPath + score + ".png");
}

setInterval(function(){
	ball.style.visibility = "visible";
	MoveBall();
	ConstrainBall();
	UpdateBall();
}, 16);
*/
