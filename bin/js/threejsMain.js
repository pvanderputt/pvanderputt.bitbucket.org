var THREE;
var canvas;
var scene;
var camera;
var renderer;

var cube;
var mouseDrag;
function InitMouseEvents(canvas) {
  var old_x, old_y;
  
  var mouseUp = function(e){
    mouseDrag = false;
  }
  
  var mouseDown = function(e){
    mouseDrag = true;
    old_x = e.pageX;
    old_y = e.pageY;
    e.preventDefault();
    return false;
  }
  
  var mouseMove = function(e){
    if (!mouseDrag) return false;
    var dX = e.pageX - old_x;
    var dY = e.pageY - old_y;
    cube.rotation.y += dX * 2 * Math.PI / canvas.width;
    cube.rotation.x += dY * 2 * Math.PI / canvas.height;
    old_x = e.pageX;
    old_y = e.pageY;
    e.preventDefault();
    return false;
  }
  
  function onWindowResize(){

		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( window.innerWidth, window.innerHeight );

	}
  
  canvas.addEventListener("mousedown", mouseDown, false);
  canvas.addEventListener("mouseup", mouseUp, false);
  canvas.addEventListener("mouseout", mouseUp, false);
  canvas.addEventListener("mousemove", mouseMove, false);
}


function MakeBox(){
	var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	
	var texture = new THREE.ImageUtils.loadTexture('bin/assets/images/crate.gif');
	texture.anisotropy = renderer.getMaxAnisotropy();

	var material = new THREE.MeshBasicMaterial( {map: texture, side:THREE.DoubleSide} );
	
	cube = new THREE.Mesh( geometry, material );
	scene.add( cube );
}

function Update(dt){
	
	if(!mouseDrag){
		cube.rotation.x += 0.03;
		cube.rotation.y += 0.03;
	}
	
}



function InitThreeJS(canvas){
	scene = new THREE.Scene();	
	camera = new THREE.PerspectiveCamera(
			75, 	canvas.clientWidth/canvas.clientHeight,  0.1,      1000);
	//		FOV		|		ratio		| 		near clip | far clip
	renderer = new THREE.WebGLRenderer({canvas: canvas});
	renderer.setSize(canvas.clientWidth, canvas.clientHeight);
	renderer.setClearColor(0x4a4991);
}

var beforeNow;
var deltaTime = 0.0;
function step() {
	requestAnimationFrame( step );
	var now = new Date().getTime();
	if (beforeNow != 0) {
		deltaTime = now - beforeNow;
		deltaTime /= 1000.0;
		
		Update(deltaTime);
	}
	beforeNow = now;
	renderer.render( scene, camera );
}

function threeMain(){
	canvas = document.getElementById("threeCanvas")
	InitMouseEvents(canvas);
	InitThreeJS(canvas);
	
	camera.position.z = 2;
	
	MakeBox();
	step();
}