function makePhoneNumber(f){
	if(f.value.length >= 10){
		f.value = f.value.replace(/\D/g, '');

		f.value = "("+f.value.slice(0,3)+")-"+f.value.slice(3,6)+"-"+f.value.slice(6,10);
	}
}

var uselessButtonCounter = 0;

function DoNothingButton(){
	uselessButtonCounter++;
	var counter = document.getElementById("nothingCounter");
	counter.textContent = uselessButtonCounter.toString();
}