Physijs.scripts.worker = "bin/lib/physijs_worker.js";
Physijs.scripts.ammo = "ammo.js";

var scene, camera, renderer;


var gameObjects = [];

function InitThreeJS(){
	scene = new Physijs.Scene;
	camera = new THREE.PerspectiveCamera(80, window.innerWidth/window.innerHeight, 0.1, 100000);
	camera.position.set(0, 10, -20);
	camera.lookAt(scene.position);
	scene.add(camera);
	
	renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setClearColor(0x4a4991);
	renderer.shadowMapEnabled = true;
	document.getElementById("threeContainer").appendChild(renderer.domElement);
	
	// Light
	var light = new THREE.DirectionalLight( 0xFFFFFF );
	light.position.set( 20, 150, -15 );
	light.target.position.copy( scene.position );
	light.castShadow = true;
	light.shadowCameraLeft = -60;
	light.shadowCameraTop = -60;
	light.shadowCameraRight = 60;
	light.shadowCameraBottom = 60;
	light.shadowCameraNear = 20;
	light.shadowCameraFar = 200;
	light.shadowBias = -.0001
	light.shadowMapWidth = light.shadowMapHeight = 2048;
	light.shadowDarkness = .7;
	scene.add( light );
}

function UpdatePhysics(dt){
	scene.simulate();
	for(var i in gameObjects){
		gameObjects[i].Update(dt);
	}
}

function Render(dt){
	renderer.render(scene, camera);
}

var lastTime;
var deltaTime = 0.0;
function StepGame(){
	requestAnimationFrame(StepGame);
	var time = new Date().getTime();
	if(lastTime !== undefined){
		deltaTime = (time - lastTime) / 1000;
		UpdatePhysics(deltaTime);
		Render(deltaTime);
	}
	lastTime = time;
}

function InitWorld(e){
	InitThreeJS();
	
	MakePlane();
	MakeBox();
	MakeBox();
	MakeBox();
	MakeBox();
	MakeBox();
	MakeBox();
	StepGame();
}

window.addEventListener("load", InitWorld, false);


function GameObject(mesh, geometry, material){
	this.mesh = mesh;
	this.geometry = geometry;
	this.material = material;
	
	this.Update = function(dt){
	}
}

function MakeBox(){
	var geometry = new THREE.BoxGeometry(2,2,2);
	var texture = new THREE.ImageUtils.loadTexture("bin/assets/images/crate.gif");
	texture.anisotropy = renderer.getMaxAnisotropy();
	var material = new THREE.MeshBasicMaterial({map: texture, side:THREE.DoubleSide});
	var cubeMesh = new Physijs.BoxMesh(geometry, material);
	cubeMesh.position.set(0, 10, 0);
	scene.add(cubeMesh);
	
	var cube = new GameObject(cubeMesh, geometry, material);
	gameObjects.push(cube);
}

function MakePlane(){
	var geometry = new THREE.BoxGeometry(500,1,500);
	var material = new THREE.MeshBasicMaterial({color: 0x008800});
	var planeMesh = new Physijs.BoxMesh(geometry, material, 0);
	//planeMesh.positon.set(0,0,0);
	planeMesh.receiveShadow = true;
	scene.add(planeMesh);
	
	var plane = new GameObject(planeMesh, geometry, material);
	gameObjects.push(plane);
}