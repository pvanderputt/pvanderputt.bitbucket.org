/*
 *  An amalgam of two WebGL tutorials.
 *  http://www.webglacademy.com/
 *  http://learningwebgl.com/
*/

if ( !window.requestAnimationFrame ) {
  window.requestAnimationFrame = ( function() {
    return window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame;
  } )();
};

/*==========Initialization==========*/

var mouseDrag = false;

function InitMouseEvents(canvas) {
  var old_x, old_y;
  
  var mouseUp = function(e){
    mouseDrag = false;
  }
  
  var mouseDown = function(e){
    mouseDrag = true;
    old_x = e.pageX;
    old_y = e.pageY;
    e.preventDefault();
    return false;
  }
  
  var mouseMove = function(e){
    if (!mouseDrag) return false;
    var dX = e.pageX - old_x;
    var dY = e.pageY - old_y;
    THETA += dX * 2 * Math.PI / canvas.width;
    PHI += dY * 2 * Math.PI / canvas.height;
    old_x = e.pageX;
    old_y = e.pageY;
    e.preventDefault();
    return false;
  }
  
  canvas.addEventListener("mousedown", mouseDown, false);
  canvas.addEventListener("mouseup", mouseUp, false);
  canvas.addEventListener("mouseout", mouseUp, false);
  canvas.addEventListener("mousemove", mouseMove, false);
}

var gl;
var gCanvas;

function InitWebGL(canvas){
  try{
    gl = canvas.getContext("experimental-webgl");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    gCanvas = canvas;
  }
  catch(e){
    if (!gl) {
      console.log("No WebGL for you :(");
    }
  }
}

var vertex_shader_source="\n\
attribute vec3 position;\n\
attribute vec3 color;\n\
uniform mat4 uWMatrix;\n\
uniform mat4 uVMatrix;\n\
uniform mat4 uPMatrix;\n\
varying vec3 vColor;\n\
void main(void) {\n\
gl_Position = uPMatrix*uVMatrix*uWMatrix*vec4(position, 1.0);\n\
vColor=color;\n\
  }";
  
var fragment_shader_source="\n\
precision mediump float;\n\
varying vec3 vColor;\n\
void main(void) {\n\
gl_FragColor = vec4(vColor, 1.);\n\
  }";

function compileShader(gl, shaderSource, shaderType){
  // Make a shader object 
  var shader = gl.createShader(shaderType);

  // Set the source code and compile
  gl.shaderSource(shader, shaderSource);
  gl.compileShader(shader);
  
  // Check to make sure it compiled correctly
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (!success) {
    console.log(shaderType + "shader didn't compile correctly: " + gl.getShaderInfoLog(shader));
    return null;
  }
  return shader;
}

function compileShaderProgram(gl, vertexShader, fragmentShader){
  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  
  gl.linkProgram(program);
  
  var success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (!success) {
    console.log("Shader program failed to link: " + gl.getProgramInfoLog(program));
    return null;
  }
  return program;
}

var shaderProgram;
function InitShaders(){
  // Compile the vertex shader
  var vertShader = compileShader(gl, vertex_shader_source, gl.VERTEX_SHADER);
  // Compile the fragment shader
  var fragShader = compileShader(gl, fragment_shader_source, gl.FRAGMENT_SHADER);
  // Link the vertex and fragment shaders together into a shader program
  shaderProgram = compileShaderProgram(gl, vertShader, fragShader);
  
  gl.useProgram(shaderProgram);
  
  // Acquire the handles to the attributes and uniforms on the shaders
  shaderProgram.position = gl.getAttribLocation(shaderProgram, "position");
  gl.enableVertexAttribArray(shaderProgram.position);
  shaderProgram.color = gl.getAttribLocation(shaderProgram, "color");
  gl.enableVertexAttribArray(shaderProgram.color);
  
  shaderProgram.uWMatrix = gl.getUniformLocation(shaderProgram, "uWMatrix");
  shaderProgram.uVMatrix = gl.getUniformLocation(shaderProgram, "uVMatrix");
  shaderProgram.uPMatrix = gl.getUniformLocation(shaderProgram, "uPMatrix");
}

var wMatrix = mat4.create();
var vMatrix = mat4.create();
var pMatrix = mat4.create();

// Set the uniforms for the shaders
function setShaderUniforms(){
  gl.uniformMatrix4fv(shaderProgram.uWMatrix, false, wMatrix);
  gl.uniformMatrix4fv(shaderProgram.uVMatrix, false, vMatrix);
  gl.uniformMatrix4fv(shaderProgram.uPMatrix, false, pMatrix);
}

function degToRad(degrees){
  return degrees * 3.14159265 / 180;
}
function radToDeg(radians){
  return radians * 180 / 3.14159265;
}

var cubeVertexBuffer;
var cubeIndexBuffer;

// Creating the cube mesh
function InitBuffers() {
  var cube_vertex=[
    -1,-1,-1,     1,1,0,
    1,-1,-1,     1,1,0,
    1, 1,-1,     1,1,0,
    -1, 1,-1,     1,1,0,
    
    -1,-1, 1,     0,0,1,
    1,-1, 1,     0,0,1,
    1, 1, 1,     0,0,1,
    -1, 1, 1,     0,0,1,
    
    -1,-1,-1,     0,1,1,
    -1, 1,-1,     0,1,1,
    -1, 1, 1,     0,1,1,
    -1,-1, 1,     0,1,1,
    
    1,-1,-1,     1,0,0,
    1, 1,-1,     1,0,0,
    1, 1, 1,     1,0,0,
    1,-1, 1,     1,0,0,
    
    -1,-1,-1,     1,0,1,
    -1,-1, 1,     1,0,1,
    1,-1, 1,     1,0,1,
    1,-1,-1,     1,0,1,
    
    -1, 1,-1,     0,1,0,
    -1, 1, 1,     0,1,0,
    1, 1, 1,     0,1,0,
    1, 1,-1,     0,1,0
    
  ];
  
  cubeVertexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube_vertex), gl.STATIC_DRAW);
  
  var cube_faces = [
    0,1,2,
    0,2,3,
    
    4,5,6,
    4,6,7,
    
    8,9,10,
    8,10,11,
    
    12,13,14,
    12,14,15,
    
    16,17,18,
    16,18,19,
    
    20,21,22,
    20,22,23
    
  ];
  cubeIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cube_faces), gl.STATIC_DRAW);
}

/*==========End Initialization==========*/

var THETA = 0;
var PHI = 0;
function webGLDraw(){
  gl.viewport(0.0, 0.0, gl.viewportWidth, gl.viewportHeight); //make my viewport sized to the correct width/height
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); //clear the viewport
  
  // create the perspective matrix
  mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);
  //create the view matrix
  mat4.identity(vMatrix);
  //create the world matrix
  mat4.identity(wMatrix);
  mat4.translate(wMatrix, [0.0, 0.0, -8.0]);
  mat4.rotateX(wMatrix, PHI);
  mat4.rotateY(wMatrix, THETA);
  
  setShaderUniforms();
  
  //set the attributes of the cube
  gl.vertexAttribPointer(shaderProgram.position, 3, gl.FLOAT, false, 4*(3+3), 0);
  gl.vertexAttribPointer(shaderProgram.color, 3, gl.FLOAT, false, 4*(3+3), 3*4);
  
  //draw the elements
  gl.drawElements(gl.TRIANGLES, 6*2*3, gl.UNSIGNED_SHORT, 0);
  
  //flush the viewport
  gl.flush();
}

var beforeNow;
var dt = 0.0;

function update(){
  var now = new Date().getTime();
  if (beforeNow != 0) {
    dt = now - beforeNow;
    dt/=1000.0;
    // Add functions to be called on update below here
    
    var horizontalSpin = 10;
    var verticalSpin = 7;
    
    if(!mouseDrag){
      THETA += (horizontalSpin) * 2 * Math.PI / gCanvas.width;
      PHI += (verticalSpin) * 2 * Math.PI / gCanvas.height;
    }
    
  }
  beforeNow = now;
}

function tick(){
  window.requestAnimationFrame(tick);
  update();
  webGLDraw();
}

function webGLMain(){
  // Get the canvas object from the page
  var canvas = document.getElementById("webGL-canvas");
  //canvas.width = window.innerWidth;
  //canvas.height = window.innerHeight;
  // Setup mouse events
  InitMouseEvents(canvas);
  // Initialize WebGL
  InitWebGL(canvas);
  // Initialize vertex and fragment shader
  InitShaders();
  // Initialize vertex and index buffers for object
  InitBuffers();
  
  gl.clearColor(0.29, 0.286, 0.569, 1.0);
  gl.enable(gl.DEPTH_TEST);
  
  tick();
}


