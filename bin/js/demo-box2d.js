if ( !window.requestAnimationFrame ) {
  window.requestAnimationFrame = ( function() {
    return window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame;
  } )();
};

var box2dDemo = (function(){
    
    var world,
        ctx,
        canvas,
        canvasWidth,
        canvasHeight,
        
        background,
        ballSheet,
        
        circleDefinition,
        boxDefinition,
        bodyDefinition,
        
        mouseClickArea,
        
        // Variables for tracking the velocity of the selected ball.
        mouseVelocity,
        numVelocities = 8,
        velocities = [],
        
        basket;
    
    function main(){
        // Create the box2d world.
        world = createWorld();
        // Get the canvas.
        canvas = document.getElementById('game');
        // Initialize the mouse and touch events.
        InitMouseEvents(canvas);
        // Get the canvas element's context.
        ctx = canvas.getContext('2d');
        
        canvasWidth = parseInt(canvas.width);
        canvasHeight = parseInt(canvas.height);
        
        InitUtilVars();
        
        InitAssets();
        
        // Initialize the demo objects
        initGame();
        
        
        
        // Run
        tick();
    }
    
    function initGame(){
        InitVelocityBuffers();
        // Create the walls and floor and ceiling.
        createBox(world, canvasWidth/2, canvasHeight+50, canvasWidth, 55, 0, true, 'wall'); // floor
        
        createBox(world, -50, 0, 55, canvasHeight*2, 0, true, 'wall'); // left wall
        createBox(world, canvasWidth+50, 0, 55, canvasHeight*2, 0, true, 'wall'); // right wall
        
        createBox(world, canvasWidth/2, -canvasHeight, canvasWidth, 50, 0, true, 'wall'); // ceiling
        
        // Create the bin wall.
        createBox(world, 120, 420, 10, 80, 0, true, 'wall'); // bin wall
        
        // Create the ramp.
        createBox(world, 380, 150, 210, 10, degToRad(-35), true, 'wall'); // ramp
        
        basket = createBox(world, 180, 270, 40, 10, 0, true, 'basket'); // the basket
        
        // Add some balls to the field
        createBall(world, 30, 250, '0');
        createBall(world, 65, 250, '1');
        createBall(world, 32, 285, '2');
        createBall(world, 68, 285, '3');
        createBall(world, 30, 320, '4');
        createBall(world, 65, 320, '5');
        createBall(world, 32, 355, '6');
        createBall(world, 68, 355, '7');
        createBall(world, 30, 390, '8');
        createBall(world, 65, 390, '9');
        createBall(world, 32, 425, '10');
    }
    
    function InitAssets(){
        background = new Image();
        background.src = 'bin/assets/images/Level_1.png';
        
        ballSheet = new Image();
        ballSheet.src = 'bin/assets/images/Ball_Sheet.png';
    }
    
    function InitUtilVars(){
        circleDefinition = new b2CircleDef();
        boxDefinition = new b2BoxDef();
        bodyDefinition = new b2BodyDef();
        
        mouseClickArea = new b2AABB();
    }
    
    function tick(){
        window.requestAnimationFrame(tick);
        update();
        draw();
    }
    
    function update() {
        var timeStep = 1.0/60;
        // iterations of collision detection / resolution?
        var iteration = 5;
        
        // Update the physics world
        world.Step(timeStep, iteration);
        // Update the gameplay
        updateGame(timeStep);
    }
    
    function draw(){
        // Clear the canvas
        //ctx.clearRect(0, 0, canvasWidth, canvasHeight);
        ctx.drawImage(background,0,0,canvasWidth,canvasHeight);
        // Draw the world
        drawWorld(world, ballSheet, ctx);
    }
    
    function updateGame(dt){
        // If we have a ball selected and the mouse is still down
        if (selectedBall != null && isMouseDown) {
            // Keep track of the old mouse positions from last frame
            oldX = selectedBall.GetBody().GetCenterPosition().x;
            oldY = selectedBall.GetBody().GetCenterPosition().y;
            
            // Set the ball's position to be the newest mouse positon
            selectedBall.GetBody().GetCenterPosition().Set(mouseX, mouseY);
            // Clear out the velocity so the ball doesn't slide out of our grasp due to gravity
            mouseVelocity.Set(0,0); // Using this variable over again
            selectedBall.GetBody().SetLinearVelocity(mouseVelocity);
            
            // Get the velocity of the object.
                // P = P0 + v*t
                // P - P0 = v*t
                // P - P0 / t = v
            mouseVelocity.Set( (mouseX - oldX) / dt , (mouseY - oldY) / dt);
            // Halve the velocity because it's a bit too fast.
            mouseVelocity.Multiply(0.5);
            // Push the velocity of this frame onto the stack of velocities.
            PushVelocity(mouseVelocity);
        }
        
        // If we have a ball selected and we need to throw it, then throw it
        if (selectedBall != null && throwBall) {
            // Get the average of the velocities of the past 10 frames for our throwing velocity.
            // This is to mitigate the problems that arise when using a trackpad.
            var throwingVelocity = AverageVelocity();
            // Set out selected ball's velocity to the calculated average.
            selectedBall.GetBody().SetLinearVelocity(throwingVelocity);
            // Now that we threw the ball, we don't want to hold it anymore.
            // So set it to null.
            selectedBall = null;
            // We just threw the ball, so set it to false.
            throwBall = false;
            // We don't have a ball anymore, so null out all our velocities.
            NullifyVelocity();
        }
        
        // Check if a ball has touched our box
        /*var contacts = basket.GetContactList();
        if (contacts != null) {
            // contacts.contact
            // contacts.next
            // contacts.prev
            // contacts.other
            contacts.other.GetCenterPosition().Set(250, 50);
        }*/
    }
    
    /**
     * Initializes the physics world.
     * @return {b2World} Returns the box2d world object that is created
     *      in this function.
     */
    function createWorld() {
        var worldAABB = new b2AABB();
        worldAABB.minVertex.Set(-1000, -1000);
        worldAABB.maxVertex.Set(1000, 1000);
        var gravity = new b2Vec2(0, 350);
        var doSleep = true;
        var world = new b2World(worldAABB, gravity, doSleep);
        return world;
    }
   
   /**
    * Creates a physics ball.
    * @param {b2World} world The box2d world object.
    * @param {number} x The x position of the ball.
    * @param {number} y The y position of the ball.
    * @return {b2Body} Returns a b2Body object.
    */
    function createBall(world, x, y, ballNum) {
        circleDefinition.density = 1.0;
        circleDefinition.radius = 15;
        circleDefinition.restitution = 0.7;
        circleDefinition.friction = 0.5;
        circleDefinition.userData = ballNum;
        
        bodyDefinition.shapes = [];
        bodyDefinition.AddShape(circleDefinition);
        bodyDefinition.position.Set(x,y);
        bodyDefinition.userData = 'ball';
        
        return world.CreateBody(bodyDefinition);
    }
   
   /**
    * Creates a physics OBB.
    * @param {b2World} world The box2d world object.
    * @param {number} x The x position of the box.
    * @param {number} y The y position of the box.
    * @param {number} width The half-width of the box.
    * @param {number} height The half-height of the box.
    * @param {number} rotation The rotation in radians of the box.
    * @param {boolean} fixed True if the box is to be a fixed object.
    * @param userData Anything that the user needs to include with the object,
    *      such as any gameplay logic that determines how it is used.
    * @return {b2Body} Returns a b2Body object.
    */
    function createBox(world, x, y, width, height, rotation, fixed, userData) {
        if (typeof(fixed) == 'undefined') fixed = true;
        
        if (!fixed) boxDefinition.density = 1.0;
        boxDefinition.userData = userData;
        boxDefinition.extents.Set(width, height);
        
        bodyDefinition.shapes = [];
        bodyDefinition.AddShape(boxDefinition);
        bodyDefinition.position.Set(x,y);
        bodyDefinition.rotation = rotation;
        
        return world.CreateBody(bodyDefinition);
    }
   
   /**
    * A function that takes degrees and returns the radians equivalent.
    * @param {number} degrees The degree value you wish to be radians.
    * @return {number} The radians value of the degrees param.
    */
    function degToRad(degrees){
        return degrees * Math.PI / 180;
    }
   
   /**
    * A function that takes radians and returns the degrees equivalent.
    * @param {number} radianss The radians value you wish to be degrees.
    * @return {number} The degrees value of the radians param.
    */
    function radToDeg(radians){
        return radians * 180 / Math.PI;
    }
   
   /**
    * Get the object that is found at the position specified.
    * @param {b2World} world The box2d world object.
    * @param {number} x The x position of the search point.
    * @param {number} y The y position of the search point.
    * @return {b2Shape} Returns the shape object of the entity at the position.
    *      Returns null if there is no object at the position.
    */
    function getObjectAtMouse(world, x, y){
        var objects = [],
           maxCount = 1;
        mouseClickArea.minVertex.Set(x - 0.5, y - 0.5);
        mouseClickArea.maxVertex.Set(x + 0.5, y + 0.5);
        world.Query(mouseClickArea, objects, maxCount);
        return objects[0];
    }
    
    function InitVelocityBuffers(){
        mouseVelocity = new b2Vec2();
        for(var i = 0; i < numVelocities; i++){
            velocities[i] = new b2Vec2(0.0, 0.0);
        }
    }
    
    /**
     * Averages the velocity of the ball for the last 10 frames.
     * @return {number} Returns the average velocity.
     */
    function AverageVelocity(){
        mouseVelocity.Set(0.0, 0.0);
        for(var i = 0; i < numVelocities; i++){
            mouseVelocity.Add(velocities[i]);
        }
        mouseVelocity.Multiply(1/numVelocities);
        
        return mouseVelocity;
    }
    
    /**
     * Sets all the velocities equal to null.
     */
    function NullifyVelocity() {
        for(var i = 0; i < numVelocities; i++){
            velocities[i].Set(0.0, 0.0);
        }
    }
    
    /**
     * Pushes a velocity onto the stack of variables. Forgets the 10th variable.
     */
    function PushVelocity(v){
        for(var i = numVelocities-1; i > 0; i--){
            velocities[i].Set(velocities[i-1].x, velocities[i-1].y);
        }
        velocities[0].Set(v.x, v.y);
    }
    
    var mouseX,             // X position of the mouse
        mouseY,             // Y position of the mouse
        oldX,               // The previous frame mouse X position
        oldY,               // The previous frame mouse Y position
        selectedBall,       // The currently selected ball
        isMouseDown,        // bool True when mouse is down
        throwBall = false;  // Bool used to throw the ball on the next frame
    
    function InitMouseEvents(canvas){
        
        var mouseDown = function(e){
            // Mouse button is now down
            isMouseDown = true;
            // Make sure we don't throw the ball yet
            throwBall = false;
            // Update the x & Y positions of the mouse
            mouseX = e.pageX;
            mouseY = e.pageY;
            e.preventDefault();
            
            // Get an object that is under the mouse
            selectedBall = getObjectAtMouse(world, mouseX, mouseY);
            
            // If it isn't null and it's a ball, then wake the object up and set its position
            if (selectedBall != null && selectedBall.GetBody().GetUserData() == 'ball') {
                selectedBall.GetBody().WakeUp();
                selectedBall.GetBody().GetCenterPosition().Set(mouseX, mouseY);
            }
            // Otherwise just set it to null
            else{
                selectedBall = null;
            }
        }
        var mouseUp = function(e){
            // Mouse is now up
            isMouseDown = false;
            // Throw the ball if we're holding one
            throwBall = true;
        }
        var mouseMove = function(e){
            // Update the mouse X & Y positions
            mouseX = e.pageX;
            mouseY = e.pageY;
            e.preventDefault();
        }
        
        var touchDown = function(e){
            // "Mouse" is now down
            isMouseDown = true;
            // Make sure we don't throw the ball yet
            throwBall = false;
            // Our mouse is the first touch point, so update it
            mouseX = e.targetTouches[0].pageX;
            mouseY = e.targetTouches[0].pageY;
            e.preventDefault();
            
            // Get and object under the finger
            selectedBall = getObjectAtMouse(world, mouseX, mouseY);
            
            // If it isn't null and it's a ball, then wake the object up and set its position
            if (selectedBall != null && selectedBall.GetBody().GetUserData() == 'ball'){
                selectedBall.GetBody().WakeUp();
                selectedBall.GetBody().GetCenterPosition().Set(mouseX, mouseY);
            }
            // Otherwise set it to null
            else{
                selectedBall = null;
            }
        }
        var touchUp = function(e){
            // Finger is off, so the mouse is "up"
            isMouseDown = false;
            // Throw the ball now
            throwBall = true;
        }
        var touchMove = function(e){
            // Update the "mouse" X & Y positions
            mouseX = e.targetTouches[0].pageX;
            mouseY = e.targetTouches[0].pageY;
            e.preventDefault();
        }
        
        
        // Add the mouse listeners
        canvas.addEventListener("mousedown", mouseDown, false);
        canvas.addEventListener("mouseup", mouseUp, false);
        canvas.addEventListener("mouseout", mouseUp, false);
        canvas.addEventListener("mousemove", mouseMove, false);
        
        // Add the touch listeners
        canvas.addEventListener("touchstart", touchDown, false);
        canvas.addEventListener("touchend", touchUp, false);
        canvas.addEventListener("touchcancel", touchUp, false);
        canvas.addEventListener("touchmove", touchMove, true);
    }
    
    return{
        run: function(){
            main();
        }
    }
    
})();