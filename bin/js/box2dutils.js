/**
 * Draws the world.
 * Taken from the box2djs demos.
 */
function drawWorld(world, spriteSheet, context) {
    for (var j = world.m_jointList; j; j = j.m_next) {
        drawJoint(j, context);
    }
    for (var b = world.m_bodyList; b; b = b.m_next) {
        for (var s = b.GetShapeList(); s != null; s = s.GetNext()) {
            drawShape(s, spriteSheet, context);
        }
    }
}

/**
 * Draws joints in the physics world.
 * Takes from the box2djs demos.
 */
function drawJoint(joint, context) {
    var b1 = joint.m_body1;
    var b2 = joint.m_body2;
    var x1 = b1.m_position;
    var x2 = b2.m_position;
    var p1 = joint.GetAnchor1();
    var p2 = joint.GetAnchor2();
    context.strokeStyle = '#00eeee';
    context.beginPath();
    switch (joint.m_type) {
    case b2Joint.e_distanceJoint:
        context.moveTo(p1.x, p1.y);
        context.lineTo(p2.x, p2.y);
        break;
 
    case b2Joint.e_pulleyJoint:
        // TODO
        break;
 
    default:
        if (b1 == world.m_groundBody) {
            context.moveTo(p1.x, p1.y);
            context.lineTo(x2.x, x2.y);
        }
        else if (b2 == world.m_groundBody) {
            context.moveTo(p1.x, p1.y);
            context.lineTo(x1.x, x1.y);
        }
        else {
            context.moveTo(x1.x, x1.y);
            context.lineTo(p1.x, p1.y);
            context.lineTo(x2.x, x2.y);
            context.lineTo(p2.x, p2.y);
        }
        break;
    }
    context.stroke();
}

/**
 * Draws shapes in the physics world.
 * Taken from the box2djs demos.
 */
function drawShape(shape, spriteSheet, context) {
    context.strokeStyle = '#000000';
    context.fillStyle = '#333333';
    
    switch (shape.m_type) {
    case b2Shape.e_circleShape:
        {
            drawBall(shape.GetPosition(), shape.GetBody().GetRotation(), shape.GetMaxRadius(), parseInt(shape.GetUserData()), spriteSheet, context);
        }
        break;
    case b2Shape.e_polyShape:
        {
            context.beginPath();
            var poly = shape;
            var tV = b2Math.AddVV(poly.m_position, b2Math.b2MulMV(poly.m_R, poly.m_vertices[0]));
            context.moveTo(tV.x, tV.y);
            for (var i = 0; i < poly.m_vertexCount; i++) {
                var v = b2Math.AddVV(poly.m_position, b2Math.b2MulMV(poly.m_R, poly.m_vertices[i]));
                context.lineTo(v.x, v.y);
            }
            context.lineTo(tV.x, tV.y);
            context.stroke();
            context.fill();
        }
        break;
    }
    
}

function drawBall(ballPos, ballRotation, ballRadius, ballNum, spriteSheet, context) {
    context.save();
    
    context.translate(ballPos.x, ballPos.y);
    
    context.rotate(ballRotation);
    
    context.drawImage(spriteSheet, ballNum*64, 0, 64, 64, -ballRadius, -ballRadius,(ballRadius*2), (ballRadius*2));
    
    context.restore();
}

