
var jsonObj;
var tabIDs = 0;

function PopulateDictionary(obj) {
    // Get the container dictionary
    var dictionary = document.getElementById("dictionary");

    MakeOuterTabs(obj, dictionary);
}

function MakeOuterTabs(obj, container) {
    // make main div for tabbed content
    var mainTabDiv = document.createElement("div");
    mainTabDiv.className = "tabbedContent";
    mainTabDiv.addEventListener("click", ShowTabContent, true);
    container.appendChild(mainTabDiv);

    // make div for tabs
    var tabDiv = document.createElement("div");
    tabDiv.className = "tabHeader";
    mainTabDiv.appendChild(tabDiv);

    // make div for contents
    var bodyDiv = document.createElement("div");
    bodyDiv.className = "tabBody";
    mainTabDiv.appendChild(bodyDiv);

    var firstVisible = true;

    // Loop through json and make tab for each beginning object
    for (var x in obj) {
        // Make tab
        var newTab = document.createElement("div");
        newTab.className = "tab";
        newTab.id = "tab" + tabIDs;

        var newTabText = document.createElement("h2");
        newTabText.className = "tabTitle";
        newTabText.textContent = x;

        newTab.appendChild(newTabText);
        tabDiv.appendChild(newTab);

        // Make tab content
        var newContent = document.createElement("div");
        newContent.className = "tabContent";
        newContent.id = "tab" + tabIDs + "content";
        
        MakeInnerTabs(obj[x], newContent)

        bodyDiv.appendChild(newContent);

        // Make the first tab visible
        if (firstVisible) {
            newContent.style.display = "block";
            newTabText.style.borderBottom = "1px solid #eeeeee";
            firstVisible = false;
        } else {
            newContent.style.display = "none";
        }
        tabIDs++;
    }
}

function MakeInnerTabs(obj, container) {
    // make main div for tabbed content
    var mainTabDiv = document.createElement("div");
    mainTabDiv.className = "innerTabbedContent";
    mainTabDiv.addEventListener("click", ShowTabContent, false);
    container.appendChild(mainTabDiv);

    // make div for tabs
    var tabDiv = document.createElement("div");
    tabDiv.className = "tabHeader";
    mainTabDiv.appendChild(tabDiv);

    // make div for contents
    var bodyDiv = document.createElement("div");
    bodyDiv.className = "tabBody";
    mainTabDiv.appendChild(bodyDiv);

    var firstVisible = true;

    // Loop through json and make tab for each beginning object
    for (var x in obj) {

        console.log(obj.x);

        // Make tab
        var newTab = document.createElement("div");
        newTab.className = "tab";
        newTab.id = "tab" + tabIDs;

        var newTabText = document.createElement("h2");
        newTabText.className = "tabTitle";
        newTabText.textContent = x;

        newTab.appendChild(newTabText);
        tabDiv.appendChild(newTab);

        // Make tab content
        var newContent = document.createElement("div");
        newContent.className = "tabContent";
        newContent.id = "tab" + tabIDs + "content";

        var newContentText = document.createElement("p");
        newContentText.className = "tabContentText";
        newContentText.textContent = obj[x];

        newContent.appendChild(newContentText);
        bodyDiv.appendChild(newContent);

        // Make the first tab visible
        if (firstVisible) {
            newContent.style.display = "block";
            newTabText.style.borderBottom = "1px solid #eeeeee";
            firstVisible = false;
        } else {
            newContent.style.display = "none";
        }
        tabIDs++;
    }
}

function DoAjax(url) {
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = HandleData;

    xhr.open("GET", url);

    xhr.send();
}

function HandleData(e) {
    if (this.readyState === 4 && this.status === 200) {
        jsonObj = JSON.parse(this.responseText);
        PopulateDictionary(jsonObj);
    }
}

window.addEventListener("load", function () {
    var url = "dictionary.txt";
    DoAjax(url);
});

function ShowTabContent(e) {
    // if we clicked the title or the tab then continue
    if (e.target.className === "tabTitle" || e.target.className === "tab") {

        var tabs = this.firstElementChild.children;
        //iterate through all the tabs to add a border
        for (var i = 0; i < tabs.length; i++) {
            // if it's the tab we're tyring to show then show it, otherwise hide it
            if (e.target.parentElement.id === tabs[i].id || e.target.id === tabs[i].id) {
                tabs[i].firstElementChild.style.borderBottom = "1px solid #eeeeee";
            } else {
                tabs[i].firstElementChild.style.borderBottom = "1px solid black";
            }
        }

        var tabContents = this.lastElementChild.children;
        // iterate through all the tabs to show/hide them
        for (var i = 0; i < tabContents.length; i++) {
            // if it's the tab we're tyring to show then show it, otherwise hide it
            if (e.target.parentElement.id + "content" === tabContents[i].id || e.target.id + "content" === tabContents[i].id) {
                tabContents[i].style.display = "block";
            } else {
                tabContents[i].style.display = "none";
            }
        }

        // Make sure that we dont hide the outer tab we want to show
        if (this.parentElement && this.parentElement.className === "tabContent") {
            this.parentElement.style.display = "block";

            // Get the outer tabs
            var outerTabs = this.parentElement.parentElement.parentElement.firstElementChild.children;
            // Iterate through the outer tabs and find the one that corresponds with this.parentElement
            for (var i = 0; i < outerTabs.length; i++) {
                if (outerTabs[i].id + "content" === this.parentElement.id) {
                    outerTabs[i].firstElementChild.style.borderBottom = "1px solid #eeeeee";
                }
            }
        }
    }
}